![Build Status](https://gitlab.com/pages/doxygen/badges/master/build.svg)

---

Example [doxygen] documentation website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Requirements](#requirements)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine

pages:
  script:
  - apk update && apk add doxygen
  - doxygen doxygen/Doxyfile
  - mv doxygen/documentation/html/ public/
  artifacts:
    paths:
    - public
  only:
  - master
```

## Requirements

- [Doxygen][]
- GraphViz (dot) [optionally]

If generating PDFs:

- PDF-LaTeX
- Ghostscript
- `make` [on *nix]

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Doxygen
1. Generate the documentation: `doxygen ./doxygen/Doxyfile`

The generated HTML will be located in the location specified by the Doxyfile,
in this case `doxygen/documentation/html`.

Read more at Doxygen's [documentation][].

## For GitLab User or Group did you want to fork this project

1. Fork the this project
1. Go to **Settings** -> **General** and expand Advanced. Then push the button **Remove
   fork relatinship** (It is important!)
1. Change **Project name**
1. Change the URL in **Project description** to
   https://yourname.gitlab.io/projectname
1. Go to **CI/CD** -> **Editor** and push the button **Commit changes**. This activate the
   pipeline configuration for CI/CD (.gitlab-cl.yml)
1. Go to **CI/CD** -> **Pipelines** and check the correkt pipeline running
1. Go to **Settings** -> **Pages** and click on button **Save** and follow the link to your doxygen home page and see
   the results. Note that the doxygen homepage has a new name space (xxxx.gitlab.io) This
   path you can using in the **Project Description**. 

Place your code in the folder `/doxygen/code`.

If changes are made in this directory, the pipeline is started automatically and the content
of doxygen is updated. If the code directory is changed, the path under INPUT in the file
`/doxygen/Doxyfile` must be adapted. 

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[doxygen]: http://www.stack.nl/~dimitri/doxygen/index.html
[install]: http://www.stack.nl/~dimitri/doxygen/manual/install.html
[documentation]: http://www.stack.nl/~dimitri/doxygen/manual/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
